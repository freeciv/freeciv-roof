#!/bin/bash
# Configuration for this game
# This file will be sourced by control.bash in the same directory.
# control.bash will have set DIR to the directory this file is in, without
# trailing slash.

# MODE will have been set to the game mode.
GAMENAMEMODE="$GAMENAME-$MODE"

# Calculate some standard paths to be used below.
ROOTPATH=`realpath "${DIR}"`
GROUNDPATH="${ROOTPATH}/ground"
HOUSEPATH="${ROOTPATH}/house"
SKYPATH="${ROOTPATH}/sky"

# Set paths.
TMPDIR="${GROUNDPATH}/tmp"
FREECIVBINARY="${GROUNDPATH}/server/bin/freeciv-server"
AUTHFCDBCONFIG="${GROUNDPATH}/games/${GAMENAMEMODE}/fc_auth.conf"
# Both of the following are for savegames.
# "snapshots" are for autosaves, e.g. every turn.
# "saves" are done by the control scripts, and happen much more frequently
# due to our long turn lengths
SNAPSHOTDIR="${HOUSEPATH}/${GAMENAMEMODE}/snapshots"
SAVEDIR="${HOUSEPATH}/${GAMENAMEMODE}/saves"
LOGDIR="${HOUSEPATH}/${GAMENAMEMODE}/"
RULESETPACK="${ROOTPATH}/ruleset"
SERVERCOMMANDFILE="${RULESETPACK}/longturn.serv"
SERVERCOMMANDFILE_NEWPLAYERS="${RULESETPACK}/newuser.serv"
SERVERCOMMANDFILE_TESTING="${RULESETPACK}/test.serv"
SERVERCOMMANDFILE_START="${RULESETPACK}/game.serv"

PLAYERSAUTOCREATE="${SKYPATH}/${GAMENAMEMODE}/players"

