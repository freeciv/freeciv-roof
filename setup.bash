#!/bin/bash

COMMAND=$1
GAMENAME=$2
DIR="${BASH_SOURCE%/*}"
DIR=`realpath $DIR`
cd $DIR


function check_gamename {
  if [[ "$GAMENAME" = "" ]]; then
    echo "GAMENAME not set!"
    print_help
    exit 1
  fi
}


function print_help {
  echo "Setup everything to host a server."
  echo "Usage:"
  echo "${BASH_SOURCE} COMMAND [GAMENAME]"
  echo "COMMANDS can be the following (asterisk means GAMENAME is required):"
  echo "help             -- output the help"
  echo "compile-server   -- compile the server"
  echo "compile-client   -- compile the client"
  echo "setup-game     * -- create all the necessary folders and files for a game"
  echo "update-ruleset   -- updates the ruleset"
}


function compile-server {
  cd freeciv && ./autogen.sh --enable-client=stub --enable-fcmp=no\
    --enable-fcdb=sqlite3 --prefix=$DIR/ground/server/ && make -j2 && make install
}


function compile-client {
  cd freeciv &&	./autogen.sh --enable-client=gtk3 --enable-fcmp=no\
    --prefix=$DIR/ground/client/ && make -j2 && make install
}


function copy_ruleset {
  cp -rv ./ruleset/longturn ./ground/server/share/freeciv/
}


function create-dirs {
  check_gamename
  mkdir --parents ./ground/tmp/
  mkdir --parents ./house/$GAMENAME-test/saves
  mkdir --parents ./house/$GAMENAME-test/snapshots
  mkdir --parents ./house/$GAMENAME-newuser/saves
  mkdir --parents ./house/$GAMENAME-newuser/snapshots
  mkdir --parents ./house/$GAMENAME-game/saves
  mkdir --parents ./house/$GAMENAME-game/snapshots
  mkdir --parents ./sky/$GAMENAME-test/
  mkdir --parents ./sky/$GAMENAME-newuser/
  mkdir --parents ./sky/$GAMENAME-game/
  mkdir --parent ./ground/games/$GAMENAME-test/
  mkdir --parent ./ground/games/$GAMENAME-newuser/
  mkdir --parent ./ground/games/$GAMENAME-game/
}


function create-fcdb-config {
  check_gamename
  cp fc_auth.conf ./ground/games/$GAMENAME-test/fc_auth.conf
  cp fc_auth.conf ./ground/games/$GAMENAME-newuser/fc_auth.conf
  cp fc_auth.conf ./ground/games/$GAMENAME-game/fc_auth.conf
  echo "  database=\"$DIR/sky/$GAMENAME-test/players.sqlite\""\
    >> ./ground/games/$GAMENAME-test/fc_auth.conf
  echo "  database=\"$DIR/sky/$GAMENAME-newuser/players.sqlite\""\
    >> ./ground/games/$GAMENAME-newuser/fc_auth.conf
  echo "  database=\"$DIR/sky/$GAMENAME-game/players.sqlite\""\
    >> ./ground/games/$GAMENAME-game/fc_auth.conf
}

function setup-game {
  check_gamename
  copy_ruleset
  create-dirs
  create-fcdb-config
  ./control.bash $GAMENAME newuser init
  ./control.bash $GAMENAME test init
  ./control.bash $GAMENAME game init
  touch ./sky/${GAMENAME}-test/players
  touch ./sky/${GAMENAME}-newuser/players
  touch ./sky/${GAMENAME}-game/players
}


case "$COMMAND" in
  help)
    print_help
    ;;
  compile-server)
    compile-server
    ;;
  compile-client)
    compile-client
    ;;
  setup-game)
    setup-game
    ;;
  update-ruleset)
    copy_ruleset
    ;;
  *)
    print_help
    ;;
esac

