# The gamename will be the part of this file's name before the ".sh".

# Port the server will listen at
PORT="5503"
NEWUSERPORT="5500"

# set this to:
# "test"       -- run server in testmode
# "newusers" -- server only for allowing new players to register
# "game"       -- actual game
