#!/bin/bash
#should run once a day or so to save disc space (the minimaps are huge)

DIR="${BASH_SOURCE%/*}"
RUNNINGGAMES="$1"

if [[ ! -f "$RUNNINGGAMES" ]]; then
  echo "File $RUNNINGGAMES not found!"
  exit 1
fi

while read -a GAMEENTRY; do
  GAMENAME="${GAMEENTRY[0]}"
  MODE="${GAMEENTRY[1]}"
  echo "GAMENAME: ${GAMENAME}"
  echo "GAMEMODE: ${MODE}"
  source "$DIR/configuration.bash"
  shopt -s nullglob
  for FILE in ${SNAPSHOTDIR}/*ppm
  do
    gzip -f "${FILE}"
  done;
done < "$RUNNINGGAMES"
