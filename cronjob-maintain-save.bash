#!/bin/bash
#should run every hour to save the current game

DIR="${BASH_SOURCE%/*}"
RUNNINGGAMES="$1"

if [[ ! -f "$RUNNINGGAMES" ]]; then
  echo "File $RUNNINGGAMES not found!"
  exit 1
fi

while read -a GAMEENTRY; do
  GAMENAME="${GAMEENTRY[0]}"
  GAMEMODE="${GAMEENTRY[1]}"
  echo "GAMENAME: ${GAMENAME}"
  echo "GAMEMODE: ${GAMEMODE}"
  bash ${DIR}/control.bash "$GAMENAME" "$GAMEMODE" maintain
  bash ${DIR}/control.bash "$GAMENAME" "$GAMEMODE" clean
done < "$RUNNINGGAMES"
