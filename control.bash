#!/bin/bash


#using set later will overwrite $1, $2 etc. so we start by saving those.
GAMENAME="$1"
MODE="$2"
OPTION="$3"
CMDSAVE="$4"
CMD="$5"


#find out where we are...
#this gets the relative path of this file, and removes the shortest occurence
#of a slash followed by stuff at the end.
DIR="${BASH_SOURCE%/*}"
DIR=`realpath $DIR`

function print_help {
  echo "This script controls the Freeciv Server"
  echo "Usage:"
  echo "${BASH_SOURCE} GAMENAME MODE COMMAND ARGS"
  echo "GAMENAME, COMMAND and MODE are required."
  echo "MODE can be newuser, test or game"
  echo "Basic configuration is read from a file GAMENAME.sh in directory $DIR"
  echo "COMMANDS ARGS can be the following:"
  echo "init      -- set up auth database and quit server again"
  echo "startnew  -- start the server without loading savefile"
  echo "start     -- start the server"
  echo "stop      -- stop the server after saving"
  echo "halt      -- stop the server without saving"
  echo "maintain  -- save if the server is running, else restart"
  echo "control   -- attach to the server process"
  echo "save      -- save if the server is running"
  echo "clean     -- clean the save directory (only 50 savegames will remain)"
  echo "command FILE CMD -- send CMD to the freeciv server, and write output to file FILE"
  echo "help      -- this help text"
  echo "info      -- output configuration and extra info"
}


#load configuration
if [[ ! -f "$DIR/$GAMENAME.sh" ]]; then
  echo "configuration $DIR/$GAMENAME.sh not found!"
  print_help
  exit -1
fi
source "$DIR/$GAMENAME.sh"
source "$DIR/configuration.bash"


#make some checks:
for DIRCHECK in "${TMPDIR}" "${SAVEDIR}" "${SNAPSHOTDIR}" "${RULESETPACK}"; do
  if [[ ! -d "$DIRCHECK" ]]; then
    echo "Error: Directory ${DIRCHECK} does not exist!"
    print_help
    exit -1
  fi
done
for FILECHECK in "${FREECIVBINARY}" "${AUTHFCDBCONFIG}" "${SERVERCOMMANDFILE}" "${SERVERCOMMANDFILE_NEWPLAYERS}" "${SERVERCOMMANDFILE_TESTING}" "${SERVERCOMMANDFILE_START}"; do
 if [[ ! -f "$FILECHECK" ]]; then
    echo "Error: File ${FILECHECK} does not exist!"
    print_help
    exit -1
  fi
done


#FIXME: change to proper argument parsing and make this
# --mode=... with default "game" or so.
if [[ "$MODE" != "newuser" && "$MODE" != "test" && "$MODE" != "game" ]]; then
  echo "Unrecognized mode: $MODE"
  print_help
  exit -1
fi


#this script better not run in parallel, so do locking.
set -e
LOCKFILE="${TMPDIR}/control.lock"
exec 200>$LOCKFILE
flock -n 200 || exit
pid=$$
echo $pid 1>&200
set +e
#after this point we can be certain no other control.bash is running.


# The folder for the scorelog doesn't seem to be configurable, so we
# just switch to the folder it should end up in.
cd $LOGDIR


#some variables
GAMENAMEMODE="$GAMENAME-$MODE"
if [[ "$MODE" = "newuser" ]]; then
  PORT=$NEWUSERPORT
fi
SERVERNAME="freeciv-$GAMENAMEMODE"
SCREENNAME="screen-${SERVERNAME}"
SERVERARGUMENTS="--exit-on-end --Announce none --port ${PORT} --auth --Database ${AUTHFCDBCONFIG} -s ${SNAPSHOTDIR} --read ${SERVERCOMMANDFILE}"
if [[ "${MODE}" = "newuser" ]]; then
  SERVERARGUMENTS="${SERVERARGUMENTS} --Newusers"
fi
#some commands for the server
SAVECOMMAND="save ${SAVEDIR}/$(date +%Y-%m-%dT%H:%M)^M"
QUITCOMMAND="quit^M"
INITAUTHCOMMAND="fcdb lua sqlite_createdb()^M"


function is_server_running {
  local RUNNING=`screen -list | grep -c ".${SCREENNAME}[[:blank:]]"`
  # If RUNNING is 0, then this means that the server is not running.
  if [[ "$RUNNING" = "0" ]] ; then
    return 1
  else
    return 0
  fi
}


function send_command_to_server {
  screen -S $SCREENNAME -p 0 -X stuff "^M$1^M"
}


#first argument: file to store answer in
#second argument: command
function send_command_to_server_with_output {
  local _CMD="$2"
  local _LOG="$1"
  if is_server_running; then
    echo "Sending command \"$_CMD\" to the server, storing result in \"$_LOG\""
    if [[ -f "$_LOG" ]]; then
      echo "Error: $_LOG already exists!"
      exit 1
    fi
    if [[ -d "$_LOG" ]]; then
      echo "Error: $_LOG already exists!"
      exit 1
    fi
    screen -S $SCREENNAME -p 0 -X logfile "$_LOG"
    screen -S $SCREENNAME -p 0 -X log on
    sleep 5
    screen -S $SCREENNAME -p 0 -X stuff "${_CMD}^M"
    sleep 5
    screen -S $SCREENNAME -p 0 -X log off
    #delete first and last line
    sed -i '1d;$d' $CMDSAVE
  else
    echo "Server is not running!"
    exit 1
  fi
}


#Starts the server. First argument is the arguments for the server.
#This function doesn't check whether the server might already be running.
#It also doesn't execute any extra commands.
function start_server_ {
  echo "Starting server..."
  local COMMAND="${FREECIVBINARY} $1"
  echo "Starting $SERVERNAME..."
  echo "Command is: $COMMAND"
  # -d -m: starts screen in detached mode
  # -S: sets session name
  screen -d -m -S "$SCREENNAME" $COMMAND
}


function wait_until_server_started {
  #FIXME: crude wait to make sure the server had time to start up.
  #better: write a function that checks whether the server
  #is actually up (i.e. not just the screen session)
  sleep 10
}


function start_server {
  if is_server_running; then
    echo "Server is already running!"
  else
    start_server_ "$1"
    if [[ "${MODE}" = "newuser" ]]; then
      wait_until_server_started
      send_command_to_server "read ${SERVERCOMMANDFILE_NEWPLAYERS}"
    elif [[ "${MODE}" = "test" ]]; then
      wait_until_server_started
      send_command_to_server "read ${SERVERCOMMANDFILE_TESTING}"
    elif [[ "${MODE}" = "game" ]]; then
      wait_until_server_started
      send_command_to_server "read ${SERVERCOMMANDFILE_START}"
    else
      echo "Unrecognized mode..."
      exit 1
    fi
  fi
}


# Stop the server (without saving!)
function stop_server {
  if is_server_running; then
    echo "Stopping server..."
    send_command_to_server "$QUITCOMMAND"
  else
    echo "Server is not running!"
  fi
}


function save_if_running {
  if is_server_running; then
    echo "Saving..."
    send_command_to_server "$SAVECOMMAND"
  else
    echo "Can't save - server isn't running!"
  fi
}


function get_savefile_info {
  # we need to find out:
  # - how many savegames we have (from those made by the cronjob)
  # - which one is the last (for loading)
  # - the two oldest ones (for deleting if we have too many)
  # Note that the glob will sort the files alphabetically.
  # We assume that alphabetic ordering corresponds to age (i.e. that older
  # savefiles come first, which can be done by having all filenames start with
  # the same prefix and then continue with a ISO 8601 date and time).
  shopt -s nullglob
  set -- ${SAVEDIR}/*
  eval "LAST_SAVE_NAME=\${$#}"
  eval "FIRST_SAVE_NAME=\${1}"
  eval "SECOND_SAVE_NAME=\${2}"
  eval "NUMBER_SAVES=$#"

  # If a first, second, or last savefile doesn't exist, then we don't want to
  # delete whatever is in these variables now. So make sure they are illegal
  # filenames in case the check before deleting later doesn't work.
  if [[ "$NUMBER_SAVES" -le "1" ]] ; then
    SECOND_SAVE_NAME=""
  fi
  if [[ "$NUMBER_SAVES" -le "0" ]] ; then
    FIRST_SAVE_NAME=""
    LAST_SAVE_NAME=""
  fi
}

get_savefile_info

function print_info {
  #get savefile info first
  get_savefile_info
  #print out configuration
  echo "# Directory this script is located in: $DIR"
  echo ""
  echo "# Configuration:"
  echo "GAMENAME: ${GAMENAME}"
  echo "PORT: ${PORT}"
  echo "MODE: ${MODE}"
  echo ""
  echo "TMPDIR: ${TMPDIR}"
  echo "SAVEDIR: ${SAVEDIR}"
  echo "SNAPSHOTDIR: ${SNAPSHOTDIR}"
  echo "SERVERNAME: ${SERVERNAME}"
  echo "SCREENNAME: ${SCREENNAME}"
  echo "FREECIVBINARY: ${FREECIVBINARY}"
  echo "AUTHFCDBCONFIG: ${AUTHFCDBCONFIG}"
  echo ""
  echo "# Savegames:"
  echo "Number of savegames stored: $NUMBER_SAVES"
  echo "Last savegame: ${LAST_SAVE_NAME}"
  echo "First savegame: ${FIRST_SAVE_NAME}"
  echo "Second savegame: ${SECOND_SAVE_NAME}"
  echo ""
  if is_server_running; then
    echo "# Server running: Yes"
  else
    echo "# Server running: No"
  fi
  echo ""
  echo "# Server commands:"
  echo "SERVERARGUMENTS: ${SERVERARGUMENTS}"
}


case "$OPTION" in
	help)
    print_help
	;;
	init)
    start_server "$SERVERARGUMENTS"
    wait_until_server_started
		echo "Server is runnning. Sending command to initialize auth database."
    send_command_to_server "$INITAUTHCOMMAND"
    echo "Sent initialization command"
    sleep 3
    stop_server
		screen -S $SCREENNAME -p 0 -X stuff "$INITAUTHCOMMAND"
		;;
	clean)
    #TODO: Make the 50 saves constant configurable
    get_savefile_info
		if [[ "$NUMBER_SAVES" -ge 51 ]];
		then
			echo "Deleting oldest two saves..."
			echo "deleting $FIRST_SAVE_NAME"
			rm -v $FIRST_SAVE_NAME
			echo "deleting $SECOND_SAVE_NAME"
			rm -v $SECOND_SAVE_NAME
		else
			echo "Nothing to clean, there are only $NUMBER_SAVES savefiles"
		fi
	;;
	startnew)
    start_server "$SERVERARGUMENTS"
    wait_until_server_started
    send_command_to_server "autocreate $PLAYERSAUTOCREATE"
    wait_until_server_started
    send_command_to_server "start"
    # Make sure that when maintain is run there
    # already is a savefile.
    save_if_running
	;;
	start)
		get_savefile_info
    if [[ "$NUMBER_SAVES" -le "0" ]];
    then
      echo "ERROR: No savefiles available!"
      exit 1
    fi
    start_server "$SERVERARGUMENTS -f ${LAST_SAVE_NAME}"
    send_command_to_server "start"
	;;
	stop)
    save_if_running
    sleep 3
    stop_server
	;;
	halt)
    stop_server
			;;
	maintain)
    if [[ "$NUMBER_SAVES" -le "0" ]];
    then
      start_server "$SERVERARGUMENTS"
      wait_until_server_started
      send_command_to_server "start"
    else
      start_server "$SERVERARGUMENTS -f ${LAST_SAVE_NAME}"
    fi
    wait_until_server_started
    save_if_running
		;;
	control)
    if is_server_running; then
      echo "Server is runnning. Attaching..."
      screen -r "$SCREENNAME"
    else
      echo "Server was not running."
    fi
		;;

	save)
    save_if_running
		;;
	command)
    send_command_to_server_with_output "$CMDSAVE" "$CMD"
		;;
  info)
    print_info
    ;;
  *)
    print_help
    ;;

esac
